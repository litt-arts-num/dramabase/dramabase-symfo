<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Traits\OriginalIdTrait;
use App\Repository\PublisherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: PublisherRepository::class)]
#[Index(columns: ["original_id"])]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class Publisher
{
    use OriginalIdTrait;
    use AuthoritiesTrait;
    use SourcesTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'publisher', targetEntity: Publication::class)]
    private Collection $publications;

    #[ORM\OneToMany(mappedBy: 'publisher', targetEntity: PublisherMembership::class)]
    private Collection $publisherMemberships;

    public function __construct()
    {
        $this->publications = new ArrayCollection();
        $this->publisherMemberships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Publication>
     */
    public function getPublications(): Collection
    {
        return $this->publications;
    }

    public function addPublication(Publication $publication): self
    {
        if (!$this->publications->contains($publication)) {
            $this->publications->add($publication);
            $publication->setPublisher($this);
        }

        return $this;
    }

    public function removePublication(Publication $publication): self
    {
        if ($this->publications->removeElement($publication)) {
            // set the owning side to null (unless already changed)
            if ($publication->getPublisher() === $this) {
                $publication->setPublisher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PublisherMembership>
     */
    public function getPublisherMemberships(): Collection
    {
        return $this->publisherMemberships;
    }

    public function addPublisherMembership(PublisherMembership $publisherMembership): self
    {
        if (!$this->publisherMemberships->contains($publisherMembership)) {
            $this->publisherMemberships->add($publisherMembership);
            $publisherMembership->setPublisher($this);
        }

        return $this;
    }

    public function removePublisherMembership(PublisherMembership $publisherMembership): self
    {
        if ($this->publisherMemberships->removeElement($publisherMembership)) {
            // set the owning side to null (unless already changed)
            if ($publisherMembership->getPublisher() === $this) {
                $publisherMembership->setPublisher(null);
            }
        }

        return $this;
    }
}
