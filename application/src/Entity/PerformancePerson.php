<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Repository\PerformancePersonRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: PerformancePersonRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class PerformancePerson
{   
    use AuthoritiesTrait;
    use SourcesTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'personParticipations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Performance $performance = null;

    #[ORM\ManyToOne(inversedBy: 'performances')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Person $person = null;

    #[ORM\ManyToOne]
    private ?Activity $activity = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerformance(): ?Performance
    {
        return $this->performance;
    }

    public function setPerformance(?Performance $performance): self
    {
        $this->performance = $performance;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }
}
