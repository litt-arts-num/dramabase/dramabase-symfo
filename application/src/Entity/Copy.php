<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\CopyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\OriginalIdTrait;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: CopyRepository::class)]
#[ApiResource]
class Copy
{

    use OriginalIdTrait;    
    use AuthoritiesTrait;
    use SourcesTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'copies')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Publication $publication = null;

    #[ORM\ManyToOne(inversedBy: 'copies')]
    private ?Library $library = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $callNumber = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // private ?string $catUri = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // private ?string $numUri = null;

    // #[ORM\Column(nullable: true)]
    // private array $authorities = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublication(): ?Publication
    {
        return $this->publication;
    }

    public function setPublication(?Publication $publication): self
    {
        $this->publication = $publication;

        return $this;
    }

    public function getLibrary(): ?Library
    {
        return $this->library;
    }

    public function setLibrary(?Library $library): self
    {
        $this->library = $library;

        return $this;
    }

    public function getCallNumber(): ?string
    {
        return $this->callNumber;
    }

    public function setCallNumber(?string $callNumber): self
    {
        $this->callNumber = $callNumber;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
    
    // public function getCatUri(): ?string
    // {
    //     return $this->catUri;
    // }

    // public function setCatUri(?string $catUri): self
    // {
    //     $this->catUri = $catUri;

    //     return $this;
    // }

    // public function getNumUri(): ?string
    // {
    //     return $this->numUri;
    // }

    // public function setNumUri(?string $numUri): self
    // {
    //     $this->numUri = $numUri;

    //     return $this;
    // }

    // public function getAuthorities(): array
    // {
    //     return $this->authorities;
    // }

    // public function setAuthorities(?array $authorities): self
    // {
    //     $this->authorities = $authorities;

    //     return $this;
    // }
}
