<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait OriginalIdTrait
{
    #[ORM\Column(nullable: true)]
    private ?int $originalId = null;


    public function getOriginalId(): ?int
    {
        return $this->originalId;
    }

    public function setOriginalId(?int $originalId): self
    {
        $this->originalId = $originalId;

        return $this;
    }
}
