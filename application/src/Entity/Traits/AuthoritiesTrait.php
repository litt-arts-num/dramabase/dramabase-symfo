<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait AuthoritiesTrait
{
    #[ORM\Column(nullable: true)]
    private array $authorities = [];


    public function getAuthorities(): array
    {
        return $this->authorities;
    }

    public function setAuthorities(?array $authorities): self
    {
        $this->authorities = $authorities;

        return $this;
    }
}
