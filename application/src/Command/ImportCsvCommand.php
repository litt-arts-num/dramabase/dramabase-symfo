<?php

namespace App\Command;

use App\Service\ImportManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import-csv',
    description: 'Add a short description for your command',
)]
class ImportCsvCommand extends Command
{
    public function __construct(ImportManager $im)
    {
        $this->im = $im;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->warning('Import en cours.');

        $this->im->init();

        $io->success('Import effectué.');

        return Command::SUCCESS;
    }
}
