<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

class LogManager
{
    public function write($line){
        $filesystem = new Filesystem();
        $filesystem->appendToFile('./data/logs.txt', $line);
    }

    public function delete(){
        $filesystem = new Filesystem();
        $filesystem->remove('./data/logs.txt');
    }
}
