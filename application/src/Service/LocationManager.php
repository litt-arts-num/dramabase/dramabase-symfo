<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\LocationType;
use App\Entity\Location;

class LocationManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function createType($originalId, $name, $description)
    {
        $locationType = new LocationType();
        $locationType->setOriginalId($originalId);
        $locationType->setName($name);
        $locationType->setDescription($description);

        $this->em->persist($locationType);

        return $locationType;
    }

    public function create($originalId, $name, $address, $startMin, $startMax, $endMin, $endMax)
    {
        $location = new Location();
        $location->setOriginalId($originalId);
        $location->setName($name);
        $location->setAddress($address);
        $location->setStartMin($startMin);
        $location->setStartMax($startMax);
        $location->setEndMin($endMin);
        $location->setEndMax($endMax);

        $this->em->persist($location);

        return $location;
    }


    public function import($lines)
    {
        echo "Import location \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);
            $address = $this->testEmpty($line[2]);

            $startMin = $this->testDate($line[3]);
            $startMax = $this->testDate($line[4]);
            $endMin = $this->testDate($line[5]);
            $endMax = $this->testDate($line[6]);

            $this->create($originalId, $name, $address, $startMin, $startMax, $endMin, $endMax);
        }
        $this->em->flush();
        $this->em->clear();
        
        // parent
        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $parentId = preg_replace('/[^0-9]/', '', $line[7]);
            $parentId = intval($parentId);

            $parent = $this->em->getRepository(Location::class)->findOneByOriginalId($parentId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($originalId);
            if ($parent) {
                $location->setParent($parent);
                $this->em->persist($location);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importLocationsLocationType($lines){
        echo "Assign location type \n";
        foreach ($lines as $line) {
            $locationId = preg_replace('/[^0-9]/', '', $line[0]);
            $locationId = intval($locationId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($locationId);

            $typeId = preg_replace('/[^0-9]/', '', $line[1]);
            $typeId = intval($typeId);
            $type = $this->em->getRepository(LocationType::class)->findOneByOriginalId($typeId);

            $location->addType($type);
            $this->em->persist($location);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }


    public function importType($lines)
    {
        echo "Import location type \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);
            $description = $this->testEmpty($line[3]);

            $this->createType($originalId, $name, $description);
        }
        $this->em->flush();
        $this->em->clear();

        // parent
        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $parentId = preg_replace('/[^0-9]/', '', $line[2]);
            $parentId = intval($parentId);

            $parent = $this->em->getRepository(LocationType::class)->findOneByOriginalId($parentId);
            $locationType = $this->em->getRepository(LocationType::class)->findOneByOriginalId($originalId);
            if ($parent) {
                $locationType->setParent($parent);
                $this->em->persist($locationType);
            }
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
