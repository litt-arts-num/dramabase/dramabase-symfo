<?php

namespace App\Service;

use App\Entity\Copy;
use App\Entity\Library;
use App\Entity\Publication;

use Doctrine\ORM\EntityManagerInterface;

class CopyManager
{
    private $em;
    private $logManager;

    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;
    }

    public function createCopy($originalId, $publication, $library, $callNumber, $description, $authorities)
    {
        $copy = new Copy();
        $copy->setOriginalId($originalId);
        $copy->setPublication($publication);
        $copy->setLibrary($library);
        $copy->setcallNumber($callNumber);
        $copy->setDescription($description);
        $copy->setauthorities($authorities);

        $this->em->persist($copy);

        return $copy;
    }

    public function importCopy($lines)
    {
        echo "Import copies \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $publicationOriginalId = preg_replace('/[^0-9]/', '', $line[1]);
            $publicationOriginalId = intval($publicationOriginalId);
            $publication = $this->em->getRepository(Publication::class)->findOneByOriginalId($publicationOriginalId);

            $libraryOriginalId = preg_replace('/[^0-9]/', '', $line[2]);
            $libraryOriginalId = intval($libraryOriginalId);
            $library = $this->em->getRepository(Library::class)->findOneByOriginalId($libraryOriginalId);

            $callNumber = $this->testEmpty($line[3]);
            $description = $this->testEmpty($line[4]);

            $authorities = $this->testEmpty($line[5]);
            // dirty trick to handle json in csv 
            $authorities = ($authorities) ? json_decode(str_replace("'", '"', $authorities), true) : [];

            $this->createCopy($originalId, $publication, $library, $callNumber, $description, $authorities);

        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
