<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em, UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function create($originalId, $signature, $firstname, $lastname, $description)
    {
        $user = new User();

        $user->setOriginalId($originalId);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setDescription($description);
        $user->setSignature($signature);
        $user->setEmail(uniqid() ."@cesar.db");

        $user->setPassword(
            $this->userPasswordHasher->hashPassword(
                $user,
                uniqid()
            )
        );

        $this->em->persist($user);

        return $user;
    }

    public function import($lines)
    {
        echo "Import users \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $signature = trim($line[1]);
            $firstname = $line[2] ? trim($line[2]) : null;
            $lastname = $line[3] ? trim($line[3]) : null;
            $description = $line[4] ? trim($line[4]) : null;

            $this->create($originalId, $signature, $firstname, $lastname, $description);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }
}
