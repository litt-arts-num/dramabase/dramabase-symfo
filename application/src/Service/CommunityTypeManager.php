<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CommunityType;

class CommunityTypeManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function create($originalId, $name, $description)
    {
        $communityType = new CommunityType();
        
        $communityType->setOriginalId($originalId);
        $communityType->setName($name);
        $communityType->setDescription($description);

        $this->em->persist($communityType);

        return $communityType;
    }

    public function import($lines)
    {
        echo "Import CommunityType \n";

        foreach ($lines as $line) {

            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);
            $name = trim($line[1]);
            $description = $this->testEmpty($line[5]);
            $this->create($originalId, $name, $description);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }


    public function importParent($lines)
    {
        echo "Import parent CommunityType \n";

        foreach ($lines as $line) {
            $communityTypeId = preg_replace('/[^0-9]/', '', $line[0]);
            $communityTypeId = intval($communityTypeId);

            $communityType = $this->em->getRepository(CommunityType::class)->findOneByOriginalId($originalId);
            $parentId = ($line[3] != "") ? trim($line[3]) : null;
            if($parentId){
                $parent = $this->em->getRepository(CommunityType::class)->findOneByOriginalId($parentId);
                if ($parent) {
                    $communityType->setParent($parent);
                    $this->em->persist($communityType);
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
    }



    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }


}
