<?php

namespace App\Service;

use App\Entity\Activity;
use App\Entity\Company;
use App\Entity\Location;
use App\Entity\Performance;
use App\Entity\PerformancePerson;
use App\Entity\Person;
use App\Entity\Script;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class PerformanceManager
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function create($originalId, $start, $end, $actNumber, $company, $script, $location)
    {
        $performance = new Performance;
        $performance->setOriginalId($originalId);
        $performance->setStart($start);
        $performance->setEnd($end);
        $performance->setActNumber($actNumber);
        $performance->setCompany($company);
        $performance->setScript($script);
        $performance->setLocation($location);

        $this->em->persist($performance);

        return;
    }

    public function createPerformancePerson($person, $performance, $activity)
    {
        $performancePerson = new PerformancePerson();
        $performancePerson->setPerson($person);
        $performancePerson->setPerformance($performance);
        $performancePerson->setActivity($activity);

        $this->em->persist($performancePerson);

        return;
    }

    public function import($lines)
    {
        echo "Import performances \n";
        $stopwatch = new Stopwatch();
        
        $batchSize = 1000;
        $batchIndex = 0;
        $stopwatch->start('perf');

        foreach ($lines as $line) {
            
            $batchIndex++;
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $scriptId = $this->testEmpty($line[1]);
            $scriptId = intval($scriptId);
            $script = ($scriptId) ? $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId) : null;
            
            $locationId = preg_replace('/[^0-9]/', '', $line[2]);
            $locationId = intval($locationId);
            $location = ($locationId) ? $this->em->getRepository(Location::class)->findOneByOriginalId($locationId) : null;

            $companyId = $this->testEmpty($line[3]);
            $companyId = intval($companyId);
            $company = ($companyId) ? $this->em->getRepository(Company::class)->findOneByOriginalId($companyId) : null;
            
            $start = $this->testDate($line[4]);
            $end = $this->testDate($line[5]);

            $actNumber = preg_replace('/[^0-9]/', '', $line[8]);
            $actNumber = intval($actNumber) != 0 ? intval($actNumber) : null;
           
            $this->create($originalId, $start, $end, $actNumber, $company, $script, $location);

            if (($batchIndex % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                echo "\t".$batchIndex."\n";
                $event = $stopwatch->stop('perf');
                echo $event ."\n\n";
                gc_collect_cycles();
                
                $stopwatch->reset();
                $stopwatch->start('perf');
            }
            
        }
        $this->em->flush();
        $this->em->clear();
        $event = $stopwatch->stop('perf');
        echo $event ."\n";
        echo "\n";
    }

    public function importPreferedTitle($lines)
    {
        echo "Import performances prefered titles \n";
        foreach ($lines as $line) {
            $performanceId = preg_replace('/[^0-9]/', '', $line[0]);
            $performanceId = intval($performanceId);
            $performance = $this->em->getRepository(Performance::class)->findOneByOriginalId($performanceId);

            $title = $this->testEmpty($line[2]);
            $subtitle = $this->testEmpty($line[3]);
            
            $performance->setTitle($title);
            $performance->setSubtitle($subtitle);

            $this->em->persist($performance);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function importAlternativeTitle($lines)
    {
        echo "Import performances alternative titles \n";
        foreach ($lines as $line) {
            $performanceId = preg_replace('/[^0-9]/', '', $line[0]);
            $performanceId = intval($performanceId);
            $performance = $this->em->getRepository(Performance::class)->findOneByOriginalId($performanceId);

            $title = $this->testEmpty($line[2]);
            $subtitle = $this->testEmpty($line[3]);
            $performance->setAlternativeTitle($title);
            $performance->setAlternativeSubtitle($subtitle);

            $this->em->persist($performance);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function importPerson($lines)
    {
        echo "Import performances persons \n";

        foreach ($lines as $line) {
            $personId = preg_replace('/[^0-9]/', '', $line[1]);
            $personId = intval($personId);
            $person = $this->em->getRepository(Person::class)->findOneByOriginalId($personId);

            $performanceId = preg_replace('/[^0-9]/', '', $line[2]);
            $performanceId = intval($performanceId);
            $performance = $this->em->getRepository(Performance::class)->findOneByOriginalId($performanceId);

            $activityId = preg_replace('/[^0-9]/', '', $line[0]);
            $activityId = intval($activityId);
            $activity = $this->em->getRepository(Activity::class)->findOneByOriginalId($activityId);

            if ($person && $performance && $activity) {
                $this->createPerformancePerson($person, $performance, $activity);
            }
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
