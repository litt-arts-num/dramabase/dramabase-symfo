<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Skill;

class SkillManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function findWithPerson() {
        $skills = [];
        $candidates = $this->em->getRepository(Skill::class)->findAll();

        foreach ($candidates as $candidate) {
            if(!$candidate->getPersons()->isEmpty() ){
                if(!in_array($candidate, $skills)) {
                    $skills[] = $candidate;
                }
                if($candidate->getParent() && !in_array($candidate->getParent(), $skills)){
                    $skills[] = $candidate->getParent();
                }
            }
        }

        usort($skills, fn($a, $b) => strcmp($a->getName(), $b->getName()));

        return $skills;
    }
    public function create($originalId, $name, $description)
    {
        $skill = new Skill();

        $skill->setOriginalId($originalId);
        $skill->setDescription($description);
        $skill->setName($name);

        $this->em->persist($skill);

        return $skill;
    }

    public function importParent($lines)
    {
        echo "Import parent skills \n";
        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $skill = $this->em->getRepository(Skill::class)->findOneByOriginalId($originalId);
            $parentOriginalId = ($line[6] != "") ? trim($line[6]) : null;
            if($parentOriginalId){
                $parent = $this->em->getRepository(Skill::class)->findOneByOriginalId($parentOriginalId);
                if ($parent) {
                    $skill->setParent($parent);
                    $this->em->persist($skill);
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function import($lines)
    {
        echo "Import skills \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);
            $name = trim($line[1]);
            $description = ($line[5] != "") ? trim($line[5]) : null;

            $this->create($originalId, $name, $description);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }
}
