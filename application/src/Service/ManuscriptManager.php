<?php

namespace App\Service;

use App\Entity\Library;
use App\Entity\Manuscript;
use App\Entity\Script;

use Doctrine\ORM\EntityManagerInterface;

class ManuscriptManager
{
    private $em;
    private $logManager;

    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;
    }

    public function createManuscript($originalId, $script, $library, $callNumber, $description)
    {
        $manuscript = new Manuscript();
        $manuscript->setOriginalId($originalId);
        $manuscript->setScript($script);
        $manuscript->setLibrary($library);
        $manuscript->setcallNumber($callNumber);
        $manuscript->setDescription($description);

        $this->em->persist($manuscript);

        return $manuscript;
    }

    public function importManuscript($lines)
    {
        echo "Import manuscripts \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $scriptOriginalId = preg_replace('/[^0-9]/', '', $line[1]);
            $scriptOriginalId = intval($scriptOriginalId);
            $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptOriginalId);

            $libraryOriginalId = preg_replace('/[^0-9]/', '', $line[2]);
            $libraryOriginalId = intval($libraryOriginalId);
            $library = $this->em->getRepository(Library::class)->findOneByOriginalId($libraryOriginalId);

            $callNumber = $this->testEmpty($line[3]);
            $description = $this->testEmpty($line[4]);

            $this->createManuscript($originalId, $script, $library, $callNumber, $description);


        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
