<?php

namespace App\Service;

use App\Entity\Library;
use App\Entity\Location;
use Doctrine\ORM\EntityManagerInterface;

class LibraryManager
{
    private $em;
    private $logManager;

    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;
    }

    public function createLibrary($originalId, $name, $location)
    {
        $library = new Library();
        $library->setOriginalId($originalId);
        $library->setName($name);
        $library->setLocation($location);


        $this->em->persist($library);

        return $library;
    }

    public function importLibrary($lines)
    {
        echo "Import libraries \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);
            $name = trim($line[1]);
            $locationOriginalId = preg_replace('/[^0-9]/', '', $line[2]);
            $locationOriginalId = intval($locationOriginalId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($locationOriginalId);

            $this->createLibrary($originalId, $name, $location);


        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
