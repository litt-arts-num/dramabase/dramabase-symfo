<?php

namespace App\Service;

use App\Entity\Company;
use App\Entity\CompanyMembership;
use App\Entity\CommunityType;
use App\Entity\Person;
use App\Entity\Activity;
use Doctrine\ORM\EntityManagerInterface;

class CompanyManager
{
    private $em;
    private $logManager;
    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;
    }

    public function create($originalId, $name, $startMin, $startMax, $endMin, $endMax, $communityType)
    {
        $company = new Company();
        $company->setOriginalId($originalId);
        $company->setStartMin($startMin);
        $company->setStartMax($startMax);
        $company->setEndMin($endMin);
        $company->setEndMax($endMax);
        $company->setName($name);
        $company->setCommunityType($communityType);

        $this->em->persist($company);

        return $company;
    }

    public function createMembership($activity, $person, $company, $startMin, $startMax, $endMin, $endMax)
    {
        $membership = new CompanyMembership();

        $membership->setActivity($activity);
        $membership->setPerson($person);
        $membership->setCompany($company);
        $membership->setStartMin($startMin);
        $membership->setStartMax($startMax);
        $membership->setEndMin($endMin);
        $membership->setEndMax($endMax);

        $this->em->persist($membership);

        return;
    }

    public function import($lines)
    {
        echo "Import companies \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);
            $startMin = $this->testDate($line[2]);
            $startMax = $this->testDate($line[3]);
            $endMin = $this->testDate($line[4]);
            $endMax = $this->testDate($line[5]);
            $communityTypeId = preg_replace('/[^0-9]/', '', $line[6]);
            $communityTypeId = intval($communityTypeId);
            $communityType = $this->em->getRepository(communityType::class)->findOneByOriginalId($communityTypeId);

            $this->create($originalId, $name, $startMin, $startMax, $endMin, $endMax, $communityType);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function importMembership($lines)
    {
        echo "Import company membership \n";

        $this->logManager->write("Import company membership \n");
        $this->logManager->write("activityId | personId | companyId \n");

        foreach ($lines as $line) {
            $activityId = $this->testEmpty($line[0]);
            $activity = $this->em->getRepository(Activity::class)->findOneByOriginalId($activityId);
            $personId = $this->testEmpty($line[1]);
            $person = $this->em->getRepository(Person::class)->findOneByOriginalId($personId);
            $companyId = $this->testEmpty($line[2]);
            $company = $this->em->getRepository(Company::class)->findOneByOriginalId($companyId);
            $startMin = $this->testDate($line[4]);
            $startMax = $this->testDate($line[5]);
            $endMin = $this->testDate($line[6]);
            $endMax = $this->testDate($line[7]);

            if(!$activity || !$person || !$company){
                $this->logManager->write($activityId . " " . $personId . " " . $companyId ."\n");
            } else {
                $this->createMembership($activity, $person, $company, $startMin, $startMax, $endMin, $endMax);
            }

        }

        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }


    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}