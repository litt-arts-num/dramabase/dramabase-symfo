<?php

namespace App\Controller;

use App\Entity\Publication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class PublicationController extends AbstractController
{
    #[Route('/publications/from/{from}/to/{to}', name: 'publications_between')]
    public function between(EntityManagerInterface $em, $from, $to): Response
    {
        $publications = $em->getRepository(Publication::class)->findBetween($from, $to);

        return $this->render('publication/list.html.twig', [
            'publications' => $publications,
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);
    }

    #[Route('/publications', name: 'publications')]
    public function index(EntityManagerInterface $em): Response
    {

        return $this->render('publication/index.html.twig');
    }


    #[Route('/publication/{id}', name: 'publication')]
    public function display(Publication $publication): Response
    {
        return $this->render('publication/display.html.twig', ['publication' => $publication]);
    }
}
