<?php

namespace App\Controller;

use App\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class LocationController extends AbstractController
{
    #[Route('/locations', name: 'locations')]
    public function index(EntityManagerInterface $em): Response
    {
        $locations = $em->getRepository(Location::class)->findAll();

        return $this->render('location/index.html.twig', ['locations' => $locations]);
    }

    #[Route('/locations/from/{from}/to/{to}', name: 'locations_between')]
    public function between(EntityManagerInterface $em, $from, $to): Response
    {
        $locations = $em->getRepository(Location::class)->findBetween($from, $to);

        return $this->render('location/index.html.twig', [
            'locations' => $locations,
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);
    }

    #[Route('/location/{id}', name: 'location')]
    public function display(Location $location): Response
    {
        return $this->render('location/display.html.twig', ['location' => $location]);
    }
}
