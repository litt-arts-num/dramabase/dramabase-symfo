<?php

namespace App\Controller;

use App\Entity\Performance;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class PerformanceController extends AbstractController
{
    #[Route('/performances', name: 'performances')]
    public function index(EntityManagerInterface $em): Response
    {

        return $this->render('performance/index.html.twig');
    }

    #[Route('/performances/from/{from}/to/{to}', name: 'performances_between')]
    public function between(EntityManagerInterface $em, $from, $to): Response
    {
        $performances = $em->getRepository(Performance::class)->findBetween($from, $to);

        return $this->render('performance/list.html.twig', [
            'performances' => $performances,
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);
    }

    #[Route('/performance/{id}', name: 'performance')]
    public function display(Performance $performance): Response
    {
        return $this->render('performance/display.html.twig', ['performance' => $performance]);
    }
}
