<?php

namespace App\Controller;

use App\Entity\Anthology;
use App\Entity\Company;
use App\Entity\Location;
use App\Entity\Performance;
use App\Entity\Person;
use App\Entity\Publication;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\DateSearchType;


class BaseController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        return $this->render('index.html.twig');
    }

    #[Route('/mentions-legales', name: 'mentions-legales')]
    public function legal(): Response
    {
        return $this->render('legal.html.twig');
    }

    #[Route('/date-search', name: 'datesearch')]
    public function dateSearch(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(DateSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $from = $form->get('startDateTime')->getData();
            $to = $form->get('endDateTime')->getData();

            $performancesCount = $em->getRepository(Performance::class)->countBetween($from, $to);
            $publicationsCount = $em->getRepository(Publication::class)->countBetween($from, $to);
            $anthologiesCount = $em->getRepository(Anthology::class)->countBetween($from, $to);
            $companiesCount = $em->getRepository(Company::class)->countBetween($from, $to);
            $personsCount = $em->getRepository(Person::class)->countBetween($from, $to);
            $locationsCount = $em->getRepository(Location::class)->countBetween($from, $to);

            return $this->render('date-results.html.twig', [
                'performancesCount' => $performancesCount,
                'publicationsCount' => $publicationsCount,
                'anthologiesCount' => $anthologiesCount,
                'companiesCount' => $companiesCount,
                'personsCount' => $personsCount,
                'locationsCount' => $locationsCount,
                'from' => $from,
                'to' => $to
            ]);
        }

        return $this->render('date-search.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/date-search/from/{from}/to/{to}', name: 'dateaccess')]
    public function dateAccess(EntityManagerInterface $em, $from, $to): Response
    {
        $from = new \DateTime($from);
        $to = new \DateTime($to);

        $performancesCount = $em->getRepository(Performance::class)->countBetween($from, $to);
        $publicationsCount = $em->getRepository(Publication::class)->countBetween($from, $to);
        $anthologiesCount = $em->getRepository(Anthology::class)->countBetween($from, $to);
        $companiesCount = $em->getRepository(Company::class)->countBetween($from, $to);
        $personsCount = $em->getRepository(Person::class)->countBetween($from, $to);
        $locationsCount = $em->getRepository(Location::class)->countBetween($from, $to);

        return $this->render('date-results.html.twig', [
            'performancesCount' => $performancesCount,
            'publicationsCount' => $publicationsCount,
            'anthologiesCount' => $anthologiesCount,
            'companiesCount' => $companiesCount,
            'personsCount' => $personsCount,
            'locationsCount' => $locationsCount,
            'from' => $from,
            'to' => $to
        ]);
    }
}
