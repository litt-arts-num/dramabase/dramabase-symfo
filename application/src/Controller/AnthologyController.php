<?php

namespace App\Controller;

use App\Entity\Anthology;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class AnthologyController extends AbstractController
{

    #[Route('/anthologies', name: 'anthologies')]
    public function index(EntityManagerInterface $em): Response
    {
        $anthologies = $em->getRepository(Anthology::class)->findAll();

        return $this->render('anthology/index.html.twig', ['anthologies' => $anthologies]);
    }

    #[Route('/anthologies/from/{from}/to/{to}', name: 'anthologies_between')]
    public function between(EntityManagerInterface $em, $from, $to): Response
    {
        $anthologies = $em->getRepository(Anthology::class)->findBetween($from, $to);

        return $this->render('anthology/list.html.twig', [
            'anthologies' => $anthologies,
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);
    }

    #[Route('/anthology/{id}', name: 'anthology')]
    public function display(Anthology $anthology): Response
    {
        return $this->render('anthology/display.html.twig', ['anthology' => $anthology]);
    }
}
