<?php

namespace App\Repository;

use App\Entity\Person;
use App\Entity\Skill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Person>
 *
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    public function save(Person $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Person $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBetween($from, $to): array
    {

        return $this->createQueryBuilder('p')
            ->andWhere('p.birthMin <= :dateTo')
            ->andWhere('p.deathMax >= :dateFrom')
            ->setParameter('dateFrom', $from)
            ->setParameter('dateTo', $to)
            ->getQuery()
            ->getResult();
    }

    public function countBetween($from, $to): int
    {

        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->andWhere('p.birthMin <= :dateTo')
            ->andWhere('p.deathMax >= :dateFrom')
            ->setParameter('dateFrom', $from)
            ->setParameter('dateTo', $to)
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    public function findWithSkill($skill): array
   {
       return $this->createQueryBuilder('person')
            ->orWhere(':skill MEMBER OF person.skills')
            ->orWhere(':skillChildren MEMBER OF person.skills')
            ->setParameter('skill', $skill)
            ->setParameter('skillChildren',  $skill->getChildren())
            ->getQuery()
            ->getResult()
       ;
   }

}
