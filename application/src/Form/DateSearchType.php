<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DateSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('startDateTime', DateType::class, [
            'mapped' => false,
            'html5' => true,
            'widget' => 'single_text',
            'placeholder' => [
                'year' => 'dzqdqz', 'month' => 'dqzdqdq', 'day' => 'dqzdqz',
            ],
            'data' => new \DateTime("15010101")
        ]);

        $builder->add('endDateTime', DateType::class, [
            'widget' => 'single_text',
            'html5' => true,
            'mapped' => false,
            'data' => new \DateTime("15991231")
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Rechercher',
            'attr' => ['class' => 'btn btn-sm btn-secondary'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
