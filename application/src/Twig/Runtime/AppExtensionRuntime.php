<?php

namespace App\Twig\Runtime;

use Twig\Extension\RuntimeExtensionInterface;

class AppExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct()
    {
        // Inject dependencies if needed
    }

    public function splitAlternatives($value)
    {
        $values = explode("/", $value);

        return $values;
    }

    public function gender($value)
    {
        if ($value == 1) {
            return "homme";
        }

        return "femme";
    }

    public function numberToRomanRepresentation($number)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function interval($min, $max)
    {
        if (!$min) {
            return "Inconnue";
        }

        $min = $min->format('Y-m-d');
        $max = $max->format('Y-m-d');

        if ($min == $max) {
            return date($min);
        }

        $minWeekday = date('j', strtotime($min));
        $minMonth = date('m', strtotime($min));
        $minYear = date('Y', strtotime($min));

        $maxWeekday = date('j', strtotime($max));
        $maxMonth = date('m', strtotime($max));
        $maxYear = date('Y', strtotime($max));

        if ($minWeekday == "1" && $minMonth == "01" && $maxWeekday == "31" && $maxMonth == "12") {
            if ($minYear === $maxYear) {
                return $minYear;
            }

            $twoFirstCharMin = mb_substr($minYear, 0, 2);
            $twoFirstCharMax = mb_substr($maxYear, 0, 2);
            $twoLastCharMin = mb_substr($minYear, -2);
            $twoLastCharMax = mb_substr($maxYear, -2);

            if ($twoLastCharMin == "00" && $twoLastCharMax == "99" && $twoFirstCharMin == $twoFirstCharMax) {
                return $this->numberToRomanRepresentation($twoFirstCharMin + 1) . "e siècle";
            }

            return 'Entre ' . $minYear . ' et ' . $maxYear;
        }

        $dayCount = cal_days_in_month(CAL_GREGORIAN, $maxMonth, $maxYear);
        if ($minWeekday == "1" && $maxWeekday == $dayCount && $minMonth == $maxMonth && $minYear === $maxYear) {
            return date('m', strtotime($min)) . "-" . $minYear;
        }

        return 'Entre ' . $min . ' et ' . $max;
    }

    public function recursiveLocationPerformances($location){
        $performances = [];

        return $this->getPerformances($location, $performances);
    }

    private function getPerformances($location, &$performances) {
        $performances = array_merge($location->getPerformances()->toArray(), $performances);
        foreach ($location->getChildren() as $child) {
           $this->getPerformances($child, $performances);
        }
        return $performances;
    }

    public function recursiveLocationParents($location){
        $parents = [];

        return $this->getParents($location, $parents);
    }

    private function getParents($location, &$parents) {
        $parent = $location->getParent();
        if ($parent) {
            $parents[] = $parent;
            $this->getParents($parent, $parents);
        }
        
        return $parents;
    }

    public function recursiveIntermediaryParents($location, $targetLocation){
        $parents = [];
        if($location != $targetLocation){
            $parents[] = $location;
        }

        return $this->getParentsUntilTarget($location, $parents, $targetLocation);
    }

    private function getParentsUntilTarget($location, &$parents, $targetLocation) {
        if($location) {
            $parent = $location->getParent();
            if ($parent && $parent != $targetLocation) {
                $parents[] = $parent;
                $this->getParentsUntilTarget($parent, $parents, $targetLocation);
            }
        }
        
        return $parents;
    }
}
