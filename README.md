# Dramabase - Symfony 6 version.

## A propos
voir [la description scientifique du projet](https://litt-arts.univ-grenoble-alpes.fr/recherche/projets-recherche/dramabase-web-donnees-letude-theatre)

## Languages & bibliothèques logicielles utilisées

-   PHP (Symfony 6)
-   Bootstrap 5

## Installation (wip)
```
git clone...
cp .env.dist .env // et éditer si besoin
cp application/.env.dist application/.env // et éditer si besoin
docker-compose up -d
docker-compose exec apache bash
  make update-dev
  exit

```

## Import old data 
```
docker-compose exec apache bash
make reimport
# have a coffee...
```

## Usage
- L'application symfony est disponible par défaut (voir `.env`) sur `http://localhost:8088`
- l'API Rest (via API Platform) est disponible sur `http://localhost:8088/api`
- Ontop et l'endpoint SPARQL sont disponibles sur `http://localhost:8086/` et `http://localhost:8086/sparql`
- graphql dispo `http://localhost:8088/api/graphql?`
  <details>
  <summary>
  Exemple de requête GraphQL
  </summary>

  {
  script(id: "/api/scripts/1") {
    id
    genre
    title
    subtitle
    alternativeTitle
    alternativeSubtitle
    forms(first: 10) {
      edges {
        node {
          test: id
          name
        }
      }
    }
    languages(first: 10) {
      edges {
        node {
          id: id
          name
        }
      }
    }
    qualifiers(last: 10) {
      edges {
        node {
          test: id
        }
      }
    }
  }
}

</details>



## Contributions
Voir [le fichier AUTHORS](AUTHORS.md)

## Licence
* Code : GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))
* Données (voir `application/data` qui contient des CSV) : Licence Creative Commons Attribution - Pas d’utilisation commerciale - Partage dans les mêmes conditions 4.0 International.